package deprecated.ManipulatingSQLQueries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class TestContextInsensitiveCookies {
    static Scanner in = new Scanner(System.in);
    public static void main(String[] args) throws SQLException {


    }

    public static void createConnection() throws SQLException {

String p1 = in.nextLine(); //bad
String p2 = "42"; //good

String x = getString(p1);
String y = getString(p2);

Connection x1 = callSQLStatement(x);
Connection y1 = callSQLStatement(y);
    }

    public static void createConnection2() throws SQLException {

        String p1 = in.nextLine(); //bad
        String p2 = "42"; //good

        String x = getString3(p1);
        String y = getString3(p2);

        Connection x1 = callSQLStatement(x);
        Connection y1 = callSQLStatement(y);
    }

    public static Connection callSQLStatement(String p) throws SQLException {
        Connection con = DriverManager.getConnection("localhost", "123", "pass");
        con.createStatement().execute("Select * from table where id = " + p);

        return con;
    }

    public static String getString(String p){
        return p;
    }

public static String getString2(String p){
    String s = p;
    p = null;
    return s;
}

    public static String getString3(String p){
        return getString2(p);
    }
}
