package deprecated.ManipulatingSQLQueries;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;

public class SQLQueries {
    static HttpServletRequest request = null;
    public static void main(String[] args) throws SQLException {
        String param = request.getParameter("userid");
        createConnection(args[0]);
        createConnection(param);
    }
    public static void createConnection(String userID) throws SQLException {
        Connection con = DriverManager.getConnection("URL", "user", "pass");
        String staticQuery = "Select * FROM 'userdata'";
        String queryOneCondition = "Select * FROM 'userdata' WHERE user = " + userID;
        String queryMultipleConditions = "Select * FROM 'userdata' WHERE user = " + userID + " AND firstName = 'AA'";
        Statement st = con.createStatement();
        String BadQueryWithStringBuilder, GoodQueryWithStringBuilder;
        int uID = 42;

        //Hier wird ein ein Stringbuilder erstellt und der Parameter wird durch den Stringbuilder in die Abfrage eingefügt.
        StringBuilder sbBAD = new StringBuilder();
        sbBAD.append("Select * FROM 'userdata' WHERE user = ");
        sbBAD.append(userID);
        sbBAD.append(" AND firstName = 'AA'");
        BadQueryWithStringBuilder = sbBAD.toString();

        //Hier wird ebenfalls ein Stringbuilde erstellt, allerdings enthält dieser keine gefährlichen Input Parameter des Benutzers.
        StringBuilder sbGOOD = new StringBuilder();
        sbGOOD.append("Select * FROM 'userdata' WHERE user = ");
        sbGOOD.append(" AND firstName = 'AA'");
        GoodQueryWithStringBuilder = sbGOOD.toString();

        st.execute(queryOneCondition); //bad -> Query is concatenated
        st.execute(staticQuery); //good -> Query is not concatenated
        st.execute(queryMultipleConditions); //bad -> Query is concatenated
        st.execute(BadQueryWithStringBuilder); //bad -> Query is concatenated with StringBuilder
        st.execute(GoodQueryWithStringBuilder); //good -> Query is concatenated with StringBuilder but without user input
        st.execute("Select * FROM 'userdata' WHERE user = " + userID); //bad -> Query is concatenated
        st.execute("Select * FROM 'userdata' WHERE user = " + uID); //bad -> Query is concatenated
        PreparedStatement ps = con.prepareStatement("Select * FROM 'userdata' WHERE user = ?"); //Good
        ps.setString(1, userID);
        ps.execute();
        getFullSQLWIthParams(queryMultipleConditions);  //Bad: Query with data from input
        getFullSQLWIthParams(staticQuery); //Good: Query without any input data
    }
    public static void getFullSQLWIthParams(String sql) throws SQLException {
        Connection con = DriverManager.getConnection("URL", "user", "pass");
        Statement st = con.createStatement();
        st.execute(sql); //Need to make sure the query is unescaped
    }
}




