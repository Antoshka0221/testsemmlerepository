package deprecated.ManipulatingSQLQueries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class TestContextInsensitive {
    static Scanner in = new Scanner(System.in);
    public static void main(String[] args) throws SQLException {


    }

    public static void createConnection() throws SQLException {

        String p1 = "UZGJNNX((199182";
        String p2 = in.nextLine();

        String x = getString(p1);
        String y = getString(p2);

        Connection x1 = establishConnectionToDB(x);
        Connection y1 = establishConnectionToDB(y);
    }

    public static void createConnection2() throws SQLException {

        String p1 = "UZGJNNX((199182";
        String p2 = in.nextLine();

        String x = getString2(p1);
        String y = getString2(p2);

        Connection x1 = establishConnectionToDB(x);
        Connection y1 = establishConnectionToDB(y);
    }

    public static Connection establishConnectionToDB(String p) throws SQLException {
        Connection con = DriverManager.getConnection("localhost", "123", p);
        return con;
    }

    public static String getString(String p){
        return p;
    }

    public static String getString2(String p){
        String s = p;
        p = null;
        return s;
    }

    public static String getString3(String p){
        return getString2(p);
    }
}
