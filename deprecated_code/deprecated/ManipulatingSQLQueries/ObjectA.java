package deprecated.ManipulatingSQLQueries;

public class ObjectA {
    public String passwort;

    public ObjectA() {
    }

    public ObjectA(String passwort) {
        this.passwort = passwort;
    }

    public String getPasswort() {
       return this.passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }
}
