package deprecated.ManipulatingSQLQueries.aliasing.aliasing;


import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple03_Bad {

    static HttpServletRequest request = null;
    public static void main(String[] args) throws SQLException {
        String userIDHardcoded = "42";
        String userIDFromParameter = request.getParameter("userID");


        ObjSQL x = new ObjSQL();
        ObjSQL z = new ObjSQL();
        x.sql = "Select * from table WHERE USERID = " + userIDHardcoded; //good
        z.sql = "Select * from table WHERE USERID = " + userIDFromParameter;; //bad


        x.f = z;
        ObjSQL v = x.f;

        Connection con = DriverManager.getConnection("127.0.0.1", "username", "d");
        con.createStatement().execute(v.sql); //Bad, because sql is from Object z.

}
}
