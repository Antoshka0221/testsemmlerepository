package deprecated.ManipulatingSQLQueries.aliasing.aliasing;


import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple07_Good {

    static HttpServletRequest request = null;
    public static void main(String[] args) throws SQLException {
        String pwHardCoded = "hshshs";
        String pwUserInput = request.getParameter("pass");


        ObjSQL x = new ObjSQL();
        ObjSQL z = new ObjSQL();
        x.sql = pwUserInput; //good
        z.sql = pwHardCoded; //bad

        ObjSQL y = x;
        y.f = z;

        Connection con = DriverManager.getConnection("127.0.0.1", "username", "d");
        con.createStatement().execute(x.sql); //Good, because sql is from Object x.

}
}
