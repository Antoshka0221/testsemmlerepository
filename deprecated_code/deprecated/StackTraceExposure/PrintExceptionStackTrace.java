package deprecated.StackTraceExposure;

import com.google.common.base.Throwables;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class PrintExceptionStackTrace {
    public static void main(String[] args) throws IOException {
    }

    public static void testMethod1(HttpServletRequest request, HttpServletResponse response, String s) throws IOException {
        try {
            System.out.println(s.length());
        } catch (NullPointerException e) {
            System.out.println("Testt");
            e.printStackTrace(response.getWriter());
            e.printStackTrace();
        }
    }

    public static void testMethod3(HttpServletRequest request, HttpServletResponse response) throws IOException {


        try {

            throw new NullPointerException("c");
        } catch (NullPointerException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string

            response.sendError(500, sStackTrace);
        }
    }

    public static void testMethod4(HttpServletRequest request, HttpServletResponse response) throws IOException {


        try {
            throw new NullPointerException("c");
        } catch (NullPointerException e) {
            ExceptionUtils.getMessage(e);
            response.sendError(0, ExceptionUtils.getMessage(e));

        }
    }


    public static void testMethod6(HttpServletRequest request, HttpServletResponse response) throws IOException {


        try {
            throw new NullPointerException("c");
        } catch (NullPointerException e) {

            response.sendError(0, getExceptionStackTrace(e));
        }

    }

    private static String getExceptionStackTrace(NullPointerException e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        System.out.println(sw.toString());
        return sw.toString();

    }


    public static void testMethod7(HttpServletRequest request, HttpServletResponse response, String s) throws IOException {
        try {
            System.out.println(s.length());
            throw new NullPointerException("c");
        } catch (NullPointerException e) {
            Throwables.getStackTraceAsString(e);
            response.sendError(0, Throwables.getStackTraceAsString(e));
        }
    }
}