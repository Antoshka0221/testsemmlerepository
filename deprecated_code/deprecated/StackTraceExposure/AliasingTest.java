package deprecated.StackTraceExposure;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AliasingTest {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {

        } catch (NullPointerException ex) {
            //Hier macht es keinen Sinn, da die Zuweisung bereits in der Exception geschieht und wir keine erneute Zuweisung benötigen.
            ex.printStackTrace(response.getWriter());
            return;
        }

        try {

        } catch (NullPointerException ex) {
            // GOOD: log the stack trace, and send back a non-revealing response
            System.out.println("Exception occurred" + ex.getStackTrace().toString());

            response.sendError(
                    HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Exception occurred");
            return;
        }
    }
}
