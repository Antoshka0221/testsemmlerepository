package deprecated.StackTraceExposure;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Intraprocedural {
    public static void directLogging(HttpServletResponse response) throws IOException {

        try {
            throw new NullPointerException("Error occurred");
        }catch (Exception e){
            e.printStackTrace(response.getWriter());
        }
    }

    public static void testMethod2(HttpServletResponse response) throws IOException {

        try {
            throw new NullPointerException("Error occurred");
        }catch (NullPointerException e){
            Exception exception = e;
            exception.printStackTrace(response.getWriter());
        }
    }

    public static void testMethod3(HttpServletResponse response) throws IOException {

try {
    throw new NullPointerException("Error occurred");
}catch (NullPointerException npe){
    RuntimeException rte= npe;
    Exception exception = rte;
    exception.printStackTrace(response.getWriter());
}
    }

    public static void testMethod4(HttpServletResponse response) throws IOException {
        Exception exception = null;
        try {
            throw new NullPointerException("Error occurred");
        }catch (NullPointerException npe){
            RuntimeException rte= npe;
            exception = rte;

        }

        exception.printStackTrace(response.getWriter());
    }
}
