package deprecated.CookiesWithoutSecureFlag;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IntraProcedural {
    static HttpServletRequest request;
    static HttpServletResponse response;
    public static void main(String[] args) {

        intraProcedural(request, response);
        intraProceduralWithArray(request, response);
        intraProceduralMultipleLevels(request, response);
        intraProceduralMultipleLevelsReferenceChange(request, response);
    }

    public static void intraProcedural(HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = new Cookie("secret", "fakesecret");


        // BAD: 'secure' flag not set
        response.addCookie(cookie);

    }

    public static void intraProceduralWithArray(HttpServletRequest request, HttpServletResponse response) {
Cookie[] cookieArray = new Cookie[2];

Cookie cookie1 = new Cookie("secret", "fakesecret");
Cookie cookie2 = new Cookie("secret", "fakesecret");
cookie2.setSecure(true);
cookieArray[0] = cookie1;
cookieArray[1] = cookie2;
// BAD: 'secure' flag not set
Cookie c = cookieArray[0];
response.addCookie(c);

    }

    public static void intraProceduralMultipleLevels(HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = new Cookie("secret", "fakesecret");
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        // BAD: 'secure' flag not set
        Cookie c2 = cookie;
        Cookie c3 = c2;
        response.addCookie(c3);



    }

    public static void intraProceduralMultipleLevelsReferenceChange(HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = new Cookie("secret", "fakesecret");
        // BAD: 'secure' flag not set
        Cookie c2 = cookie;
        Cookie c3 = c2;
        c3.setSecure(true);
        response.addCookie(c3);



    }

    public static void intraProceduralMultipleLevelsReferenceChangeWithException(HttpServletRequest request, HttpServletResponse response) {


        Cookie cookie = new Cookie("secret", "fakesecret");
        cookie.setSecure(false);
        try{
            throw new NullPointerException("");
        }catch (Exception e){
            cookie.setSecure(true);
        }

        Cookie c2 = cookie;
        Cookie c3 = c2;
        response.addCookie(c3);
    }


}
