package deprecated.CookiesWithoutSecureFlag;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class InterProcedural {

    public void testMethod(HttpServletResponse response) {
        Cookie cookie1 = new Cookie("secret", "fakesecret");
        Cookie cookie2 = new Cookie("secret", "fakesecret");
        setSecureTrue(cookie2);
        response.addCookie(cookie1);
    }

    public void testMethod2(HttpServletResponse response) {
        Cookie cookie1 = getNewCookie();
        setSecureTrue(cookie1);
        response.addCookie(cookie1);
    }

    public void testMethod3(HttpServletResponse response) {
Cookie cookie1 = getNewCookie();
Cookie cookie2 = getNewCookie();
setSecureTrue(cookie2);
setSecureFalse(cookie1);
cookie1 = getCookie(cookie1);

response.addCookie(cookie1);
    }

    public Cookie getNewCookie(){
        return new Cookie("secret", "fakseSecret");
    }

    public Cookie getCookie(Cookie cookie){
        return getCookie2(cookie);
    }

    private Cookie getCookie2(Cookie cookie) {
        return getCookie3(cookie);
    }

    private Cookie getCookie3(Cookie cookie) {
        return cookie;
    }

    public void setSecureTrue(Cookie cookie) {
        cookie.setSecure(true);
    }

    public void setSecureFalse(Cookie cookie) {
        cookie.setSecure(false);
    }
}
