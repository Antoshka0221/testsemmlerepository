package deprecated.CookiesWithoutSecureFlag;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecureCookies {

    public static void main(String[] args) {

    }

    public static void test(HttpServletRequest request, HttpServletResponse response) {

        {
            Cookie cookie = new Cookie("secret", "fakesecret");
            // GOOD: set 'secure' flag
            cookie.setSecure(true);
            response.addCookie(cookie);
        }

        {
            Cookie cookie = new Cookie("secret", "fakesecret");
            // BAD: 'secure' flag not set
            response.addCookie(cookie);
        }



        testCookie2(new Cookie("", ""), response);
        testCookie3(new Cookie("", ""), response);
        testCookie4(new Cookie("", ""), response);
        testCookie5(new Cookie("", ""), response);
        testCookie6(new Cookie("", ""), response);
        testCookie7(new Cookie("", ""), response);
        testCookie8(new Cookie("", ""), response);
        testCookie6(response);

    }

    public static void testCookie2(Cookie cookie, HttpServletResponse response){
        response.addCookie(returnCookie(cookie));
        setSecureToTrue(cookie);
        response.addCookie((cookie));
        setSecureToFalse(cookie);
        response.addCookie(cookie);
        setSecureToTrue(cookie);
        response.addCookie(cookie);
    }

    public static void testCookie3(Cookie cookie, HttpServletResponse response){
        setSecureToTrue(cookie);
        response.addCookie((cookie));
        setSecureToFalse(cookie);
        response.addCookie(cookie);
        setSecureToTrue(cookie);
        response.addCookie(cookie);
    }

    public static void testCookie4(Cookie cookie, HttpServletResponse response){
        cookie.setSecure(true);
        cookie.setSecure(false);
        setSecureToTrue(cookie);
        response.addCookie(cookie);

    }

    public static void testCookie7(Cookie cookie, HttpServletResponse response){
        cookie.setSecure(true);
        cookie.setSecure(false);
        response.addCookie(cookie);

    }

    public static void testCookie8(Cookie cookie, HttpServletResponse response){
        cookie.setSecure(true);
        cookie.setSecure(false);
        cookie.setSecure(true);
        response.addCookie(cookie);

    }

    public static void testCookie5(Cookie cookie, HttpServletResponse response){
        setSecureToTrue(cookie);
        response.addCookie(cookie);

    }

    public static void testCookie6(HttpServletResponse response){
        Cookie cookie = new Cookie("", "");
        setSecureToTrue(cookie);
        response.addCookie(cookie);

    }

    public static void testCookie9(HttpServletResponse response){
        Cookie cookie = new Cookie("", "");
        response.addCookie(cookie);
        cookie.setSecure(true);

    }

    public static void testCookie10(HttpServletResponse response){
        Cookie cookie = new Cookie("", "");
        System.out.println("Cookie created");
        cookie.getSecure();
        doSomethingElse();
        cookie.setSecure(true);
        response.addCookie(cookie);
    }

    private static void doSomethingElse() {
        System.out.println("Called method DoSomethingElse");
    }

    public static void testCookie6(Cookie cookie, HttpServletResponse response){
        setSecureToFalse(cookie);
        response.addCookie(cookie);

    }

    public static Cookie returnCookie(Cookie cookie){
        return cookie;
    }

    public static void setSecureToTrue(Cookie cookie){
        cookie.setSecure(true);
    }

    public static void setSecureToFalse(Cookie cookie){
        cookie.setSecure(false);
    }
}
