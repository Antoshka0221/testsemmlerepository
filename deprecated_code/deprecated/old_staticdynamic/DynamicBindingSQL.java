package deprecated.old_staticdynamic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DynamicBindingSQL {

    public static void main(String[] args) throws SQLException {
        Z a = new A();
        Z b = new B();
        Z c = new C();
        Z d = new D();
        E e = new E(d);
        Connection conA = DriverManager.getConnection("URL", "user", a.getPswd());
        Connection conB = DriverManager.getConnection("URL", "user", b.getPswd());
        Connection conC = DriverManager.getConnection("URL", "user", c.getPswd());
        Connection conD = DriverManager.getConnection("URL", "user", d.getPswd());
        Connection conE = DriverManager.getConnection("URL", "user", e.getPswd());





    }

}
