package deprecated.NoSSLUsage;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import java.net.URL;
import java.net.URLConnection;


public class SSLUsage {

    public static void main(String[] args) throws IOException {
        testI();
        URL urlHTTP = new URL("http://localhost:1056/api/auth/login");
        URL urlHTTPS = new URL("https://localhost:1056/api/auth/login");

        URLConnection c1 = openConnetionCustom1(urlHTTP);
        URLConnection c2 = openConnetionCustom1(urlHTTPS);
        c1.connect();
        c2.connect();
        OutputStream os1 = c1.getOutputStream();
        OutputStream os2 = c1.getOutputStream();

        URLConnection c3 = openConnetionCustom2(urlHTTP);
        URLConnection c4 = openConnetionCustom2(urlHTTPS);
        c3.connect();
        c4.connect();
        OutputStream os3 = c1.getOutputStream();
        OutputStream os4 = c1.getOutputStream();

        URLConnection c5 = openConnetionCustom3(urlHTTP);
        URLConnection c6 = openConnetionCustom3(urlHTTPS);
        c5.connect();
        c6.connect();
        OutputStream os5 = c1.getOutputStream();
        OutputStream os6 = c1.getOutputStream();

        URLConnection c7 = openConnetionCustom4(urlHTTP);
        URLConnection c8 = openConnetionCustom4(urlHTTPS);
        c7.connect();
        c8.connect();
        OutputStream os7 = c1.getOutputStream();
        OutputStream os8 = c1.getOutputStream();

        URLConnection c9 = openConnetionCustom5(urlHTTP);
        URLConnection c10 = openConnetionCustom5(urlHTTPS);
        c9.connect();
        c10.connect();
        OutputStream os9 = c1.getOutputStream();
        OutputStream os10 = c1.getOutputStream();


        URLConnection c11 = openConnetionCustom6(urlHTTP);
        URLConnection c12= openConnetionCustom6(urlHTTPS);
        c11.connect();
        c12.connect();
        OutputStream os11 = c1.getOutputStream();
        OutputStream os12 = c1.getOutputStream();



        HttpURLConnection c14 = openConnetionCustom4(urlHTTP);
        HttpURLConnection c15 = openConnetionCustom4(urlHTTPS);
        c15.connect();
        c15.connect();
        OutputStream os14 = c1.getOutputStream();
        OutputStream os15 = c1.getOutputStream();

        HttpURLConnection c16 = openConnetionCustom5(urlHTTP);
        HttpURLConnection c17 = openConnetionCustom5(urlHTTPS);
        c16.connect();
        c17.connect();
        OutputStream os16 = c1.getOutputStream();
        OutputStream os17 = c1.getOutputStream();


        HttpURLConnection c18 = openConnetionCustom6(urlHTTP);
        HttpURLConnection c19= openConnetionCustom6(urlHTTPS);
        c18.connect();
        c19.connect();
        OutputStream os18 = c1.getOutputStream();
        OutputStream os19 = c1.getOutputStream();


        HttpsURLConnection c20 = openConnetionCustom6(urlHTTP);
        HttpsURLConnection c21 = openConnetionCustom6(urlHTTPS);
        c20.connect();
        c21.connect();
        OutputStream os20 = c1.getOutputStream();
        OutputStream os21 = c1.getOutputStream();



    }

    public static int testI(){
        int i = 0;
        return i++;
    }


    //BAD Https is Downgraded to
    public void openConnection1() throws IOException {
        try {
            URL u = new URL("http://www.secret.example.org/");
            HttpURLConnection httpcon = (HttpURLConnection) u.openConnection();
            httpcon.setRequestMethod("PUT");
            httpcon.connect();
            // BAD: output stream from non-HTTPS connection
            OutputStream os = httpcon.getOutputStream();
            httpcon.disconnect();
        }
        catch (IOException e) {
            // fail
        }
    }

    public void openConnection2() throws IOException {
        try {
            URL u = new URL("http://www.secret.example.org/");
            HttpURLConnection httpcon = (HttpsURLConnection) u.openConnection();
            httpcon.setRequestMethod("PUT");
            httpcon.connect();
            // BAD: output stream from non-HTTPS connection
            OutputStream os = httpcon.getOutputStream();
            httpcon.disconnect();
        }
        catch (IOException e) {
            // fail
        }
    }

    public void openConnection3() throws IOException {
        try {
            URL u = new URL("http://www.secret.example.org/");
            HttpsURLConnection httpcon = (HttpsURLConnection) u.openConnection();
            httpcon.setRequestMethod("PUT");
            httpcon.connect();
            // BAD: output stream from non-HTTPS connection
            OutputStream os = httpcon.getOutputStream();
            httpcon.disconnect();
        }
        catch (IOException e) {
            // fail
        }
    }

    public void openConnection4() throws IOException {
        try {
            URL u = new URL("https://www.secret.example.org/");
            HttpURLConnection httpcon = (HttpURLConnection) u.openConnection();
            httpcon.setRequestMethod("PUT");
            httpcon.connect();
            // BAD: output stream from non-HTTPS connection
            OutputStream os = httpcon.getOutputStream();
            httpcon.disconnect();
        }
        catch (IOException e) {
            // fail
        }
    }

    public void openConnection5() throws IOException {
        try {
            URL u = new URL("https://www.secret.example.org/");
            HttpURLConnection httpcon = (HttpsURLConnection) u.openConnection();
            httpcon.setRequestMethod("PUT");
            httpcon.connect();
            // BAD: output stream from non-HTTPS connection
            OutputStream os = httpcon.getOutputStream();
            httpcon.disconnect();
        }
        catch (IOException e) {
            // fail
        }
    }

    public void openConnection6() throws IOException {
        try {
            URL u = new URL("https://www.secret.example.org/");
            HttpsURLConnection httpcon = (HttpsURLConnection) u.openConnection();
            httpcon.setRequestMethod("PUT");
            httpcon.connect();
            // BAD: output stream from non-HTTPS connection
            OutputStream os = httpcon.getOutputStream();
            httpcon.disconnect();
        }
        catch (IOException e) {
            // fail
        }
    }

    public static URLConnection openConnetionCustom1(URL url) throws IOException {
        return url.openConnection();
    }

    public static URLConnection openConnetionCustom2(URL url) throws IOException {
        return (HttpsURLConnection)url.openConnection();
    }

    public static URLConnection openConnetionCustom3(URL url) throws IOException {
        return (HttpURLConnection)url.openConnection();
    }

    public static HttpURLConnection openConnetionCustom4(URL url) throws IOException {
        return (HttpURLConnection)url.openConnection();
    }

    public static HttpURLConnection openConnetionCustom5(URL url) throws IOException {
        return (HttpsURLConnection)url.openConnection();
    }

    public static HttpsURLConnection openConnetionCustom6(URL url) throws IOException {
        return (HttpsURLConnection)url.openConnection();
    }




}
