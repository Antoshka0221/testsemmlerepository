package deprecated.HardcodedPasswordField.aliasing;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AliasingTest {


        static String URL = "localhost:8080/dbms";

        public static void main(String[] args) throws SQLException, IOException {

            final String pw = "pass12eweww3";
            final String username = "user11kkk1";

            PasswordObject p = new PasswordObject();

            PasswordObject p2 = p;
            PasswordObject p3 = p2;
            PasswordObject p4= p;

            p2.password = pw;
            p.password = pw;
            p3.password = pw;

            Connection con = DriverManager.getConnection(URL, username, p.password);
            Connection con2 = DriverManager.getConnection(URL, username, p2.password);
            Connection con3 = DriverManager.getConnection(URL, username, p3.password);
            Connection con4 = DriverManager.getConnection(URL, username, p4.password);



            try {
                con.createStatement();
            } finally {
                con.close();
            }
        }

    }

