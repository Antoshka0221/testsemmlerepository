package deprecated.HardcodedPasswordField;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;


public class HardCodedPassword {
    static HttpServletRequest request;
    static HttpServletResponse response;

    static String URL = "localhost:8080/dbms";

    public static void main(String[] args) throws SQLException, IOException {


        loadPWFromPropertiesFile();
        loadPWFromUserInput();




String username = "user";

establishConnection(URL, username, getPassword1());
establishConnection(URL, username, getPassword2());
establishConnection(URL, username, getPassword3());
establishConnection(URL, username, getPassword4());
establishConnection(URL, username, getPassword5());
establishConnection(URL, username, getPassword6());

establishConnectionToLevel_3(URL, "useer1111", getPassword1()); //Bad



loginIntoFacebook(); //Bad: hardcoded pass in method
loginIntoFacebookWithParameters(username, getPassword1()); //bad: call with hardcoded pass
loginIntoFacebookWithParameters(username, getPassword2()); //bad: call with hardcoded pass
loginIntoFacebookWithParameters(username, getPassword3()); //bad: call with hardcoded pass
loginIntoFacebookWithParameters(username, getPassword4()); //bad: call with hardcoded pass
loginIntoFacebookWithParameters(username, getPassword5()); //bad: call with hardcoded pass
loginIntoFacebookWithParameters(username, getPassword6()); //bad: call with hardcoded pass

//bad: call with hardcoded user credentials

comparisonOfPasswordFromArguments(args);
comparisonOfPasswordFromRequest(request, response);
comparisonOfPasswordFromScanner();
    }

    public static void comparisonOfPasswordFromRequest(HttpServletRequest request, HttpServletResponse response){
        if(request.getParameter("pass").equals(getPassword1())){
            response.setStatus(200);
        }else {
            response.setStatus(401);
        }
    }

    public static boolean comparisonOfPasswordFromScanner(){
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter pass");

        String c = myObj.nextLine();  // Read user input

        if( c.equals("POadsfs4z8882832738 ")){
            return true;
        }else {
            return false;
        }
    }

    public static boolean comparisonOfPasswordFromArguments(String args[]){
        if( args[0].equals("POadsfs4z8882832738 ")){
            return true;
        }else {
            return false;
        }
    }




    public static void loginIntoFacebook() throws IOException {
        final WebClient webClient = new WebClient();
        final HtmlPage page1 = webClient.getPage("http://www.facebook.com");
        final HtmlForm form = page1.getFormByName("login_form");

        final HtmlTextInput textField = form.getInputByName("email");
        textField.setValueAttribute("jon@jon.com");
        final HtmlTextInput passField = form.getInputByName("pass");
        passField.setValueAttribute("ahhhh");
        final HtmlInput button = form.getInputsByValue("Log in").get(0);
        final HtmlPage page2 = button.click();
    }

    public static void loginIntoFacebookWithParameters(String u, String p) throws IOException {
        final WebClient webClient = new WebClient();
        final HtmlPage page1 = webClient.getPage("http://www.facebook.com");
        final HtmlForm form = page1.getFormByName("login_form");

        final HtmlTextInput textField = form.getInputByName("email");
        textField.setValueAttribute(u);
        final HtmlTextInput passField = form.getInputByName("pass");
        passField.setValueAttribute(p);
        final HtmlInput button = form.getInputsByValue("Log in").get(0);
        final HtmlPage page2 = button.click();
    }

    public static void establishConnection(String url, String u, String p) throws SQLException {
        Connection c = DriverManager.getConnection(url, u, p);
        c.prepareStatement("SELECT * FROM TABLE");
        c.close();
    }

    public static void establishConnectionToLevel_3(String url, String u, String p) throws SQLException {
        establishConnectionLevel_2(url, u, p);
    }

    public static void establishConnectionLevel_2(String url, String u, String p) throws SQLException {
        establishConnection(url, u, p);
    }


    //GOOD: The pass is loaded from a properties file.
    public static void loadPWFromPropertiesFile() throws IOException, SQLException {
        InputStream input = new FileInputStream("src/main/resources/connection.properties");

        Properties prop = new Properties();

        // load a properties file
        prop.load(input);

        // get the property value and print it out
        String user = prop.getProperty("db.user");
        String password = prop.getProperty("db.password");

        //pass pass from variables
        Connection con = DriverManager.getConnection(URL, user, password);
        con.createStatement();
        con.close();
    }


    public static void loadPWFromUserInput() throws IOException, SQLException {

        Scanner in = new Scanner(System.in);
        System.out.println("Please enter DB User");
        String user = in.nextLine();

        System.out.println("Please enter DB Password");
        String password = in.nextLine();

        Connection con = DriverManager.getConnection(URL, user, password);
        con.createStatement();
        con.close();

    }


    //BAD: Password is hardcoded and returned by method
    public static String getPassword1(){
        return "passwort";
    }

    //BAD: Password is hardcoded and returned by method
    public static String getPassword2(){
        return "passwort".toString();
    }

    //BAD: Password is hardcoded and returned by method using StringBuilder
    public static String getPassword3(){

        StringBuilder sb = new StringBuilder();
        sb.append("Ts");
        sb.append("Tss");
        sb.append("Tssd");
        return sb.toString();
    }

    //BAD: Password is hardcoded and returned by method using StringBuilder and simple String operations
    public static String getPassword4(){

        StringBuilder sb = new StringBuilder();
        sb.append("Ts");
        sb.append("Tss");
        sb.append("Tssd");
        return StringUtils.reverse(sb.toString());
    }

    //BAD: Password is hardcoded in separate class and returned by method using StringBuilder
    public static String getPassword5(){

        StringBuilder sb = new StringBuilder();
        sb.append(getPassword6());
        sb.append("Tss");
        sb.append("Tssd");
        return sb.toString();
    }

    //BAD: Password is hardcoded and returned by method
    public static String getPassword6(){
        return HardCodedPassword2.notiz;
    }
}