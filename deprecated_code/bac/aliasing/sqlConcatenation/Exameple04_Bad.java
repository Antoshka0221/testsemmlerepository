package bac.aliasing.sqlConcatenation;



import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple04_Bad {

    static HttpServletRequest request = null;
    public static void main(String[] args) throws SQLException {
        String badID = request.getParameter("id");
        String goodID = "42";

        ObjSQL x = new ObjSQL();
        ObjSQL z = new ObjSQL();
        x.sql = goodID; //good
        z.sql = badID; //bad

        ObjSQL y = x;
        y.f = z;
        ObjSQL v = y.f;


        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pas");
        con.createStatement().execute("SELECT * FROM TABLE WHERE ID = " + v.sql);

}
}
