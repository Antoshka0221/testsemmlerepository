package bac.aliasing.sqlConcatenation;



import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple02_Good {

    static HttpServletRequest request = null;

    public static void main(String[] args) throws SQLException {
        String badID = request.getParameter("id");
        String goodID = "42";

        ObjSQL o1 = new ObjSQL();
        ObjSQL o2 = new ObjSQL();
        o1.sql = goodID; //good
        o2.sql = badID; //bad

        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pas");
        con.createStatement().execute("SELECT * FROM TABLE WHERE ID = " + o1.sql);
        //Bad, because passowrd is from Obj z.

    }
}
