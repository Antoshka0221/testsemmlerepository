package bac.aliasing.hardcodedPassword;


import javax.servlet.http.HttpServletRequest;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple06_Bad {

    static HttpServletRequest request = null;
    public static void main(String[] args) throws SQLException {
        String pwHardCoded = "hshshs";
        String pwUserInput = request.getParameter("pass");


        Obj x = new Obj();
        Obj z = new Obj();
        x.password = pwUserInput; //good
        z.password = pwHardCoded; //bad

        Obj w = x;
        Obj y = x;
        y.f = z;
        Obj v = w.f;

        DriverManager.getConnection("127.0.0.1", "username", v.password); //Bad, because passowrd is from Obj z.


}
}
