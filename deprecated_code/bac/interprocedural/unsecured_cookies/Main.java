package bac.interprocedural.unsecured_cookies;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class Main {
    static HttpServletResponse response;
    public static void main(String[] args) {
        testMethod();
        testMethod2();
        testMethod3();
    }
    public static void testMethod() {
        Cookie cookie1 = new Cookie("secret", "fakesecret");
        Cookie cookie2 = new Cookie("secret", "fakesecret");
        setSecureTrue(cookie2);
        response.addCookie(cookie1);
    }

    public static void testMethod2() {
        Cookie cookie1 = getNewCookie();
        setSecureTrue(cookie1);
        response.addCookie(cookie1);
    }

    public static void testMethod3() {
        Cookie cookie1 = getNewCookie();
        Cookie cookie2 = getNewCookie();
        setSecureTrue(cookie2);
        setSecureFalse(cookie1);
        cookie1 = getCookie(cookie1);

        response.addCookie(cookie1);
    }

    public static Cookie getNewCookie(){
        return new Cookie("secret", "fakseSecret");
    }

    public static Cookie getCookie(Cookie cookie){
        return getCookie2(cookie);
    }

    private static Cookie getCookie2(Cookie cookie) {
        return getCookie3(cookie);
    }

    private static Cookie getCookie3(Cookie cookie) {
        return cookie;
    }

    public static void setSecureTrue(Cookie cookie) {
        cookie.setSecure(true);
    }

    public static void setSecureFalse(Cookie cookie) {
        cookie.setSecure(false);
    }

}
