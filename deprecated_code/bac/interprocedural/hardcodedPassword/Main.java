package bac.interprocedural.hardcodedPassword;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {

    static HttpServletRequest request;
    static HttpServletResponse response;

    static String URL = "localhost:8080/dbms";

    public static void main(String[] args) throws SQLException {


        String username = "user";

        establishConnection(URL, username, getPassword1());
        establishConnection(URL, username, getPassword2());
        establishConnection(URL, username, getPassword3());
        establishConnection(URL, username, getPassword4());
        establishConnection(URL, username, getPassword5());
        establishConnection(URL, username, getPassword6());

        establishConnectionToLevel_3(URL, "useer1111", getPassword1()); //Bad



    }

    public static void establishConnectionToLevel_3(String url, String u, String p) throws SQLException {
        establishConnectionLevel_2(url, u, p);
    }

    public static void establishConnectionLevel_2(String url, String u, String p) throws SQLException {
        establishConnection(url, u, p);
    }

    public static void establishConnection(String url, String u, String p) throws SQLException {
        Connection c = DriverManager.getConnection(url, u, p);
        c.prepareStatement("SELECT * FROM TABLE");
        c.close();
    }

    public static String getPassword1(){
        return "passwort";
    }

    //BAD: Password is hardcoded and returned by method
    public static String getPassword2(){
        return "passwort".toString();
    }

    //BAD: Password is hardcoded and returned by method using StringBuilder
    public static String getPassword3(){

        StringBuilder sb = new StringBuilder();
        sb.append("Ts");
        sb.append("Tss");
        sb.append("Tssd");
        return sb.toString();
    }

    //BAD: Password is hardcoded and returned by method using StringBuilder and simple String operations
    public static String getPassword4(){

        StringBuilder sb = new StringBuilder();
        sb.append("Ts");
        sb.append("Tss");
        sb.append("Tssd");
        return StringUtils.reverse(sb.toString());
    }

    //BAD: Password is hardcoded in separate class and returned by method using StringBuilder
    public static String getPassword5(){

        StringBuilder sb = new StringBuilder();
        sb.append(getPassword6());
        sb.append("Tss");
        sb.append("Tssd");
        return sb.toString();
    }

    //BAD: Password is hardcoded and returned by method
    public static String getPassword6(){
        return HardCodedPassword2.notiz;
    }

}
