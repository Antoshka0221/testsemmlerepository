package bac.contextSensitive.sqlConcatenation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class TestContextInsensitive {
    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {
        createConnection();
        createConnection2();
    }

    public static void createConnection() throws SQLException {

        String p1 = "UZGJNNX((199182";
        String p2 = in.nextLine();

        Connection con = DriverManager.getConnection("localhost", "123", "sds");
        con.createStatement().execute("SELECT * from table where userID = " + p2);

        String x = getString(p1);
        String y = getString(p2);

        executeQuery(x);
        executeQuery(y);
    }

    public static void createConnection2() throws SQLException {

        String p1 = "42"; //good
        String p2 = in.nextLine(); //bad

        String x = getString3(p1); //good
        String y = getString3(p2); //bad

        executeQuery(x); //good
        executeQuery(y); //bad


    }


    public static Connection executeQuery(String p) throws SQLException {
        Connection con = DriverManager.getConnection("localhost", "123", "sds");
        con.createStatement().execute("SELECT * from table where userID = " + p);
        return con;
    }

    public static String getString(String p) {
        return p;
    }

    public static String getString2(String p) {
        String s = p;
        p = null;
        return s;
    }

    public static String getString3(String p) {
        return getString2(p);
    }
}
