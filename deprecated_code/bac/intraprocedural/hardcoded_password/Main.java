package bac.intraprocedural.hardcoded_password;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@SuppressWarnings("Duplicates")
public class Main {
    static String URL = "localhost:8080/dbms";
    static HttpServletRequest request;
    public static void main(String[] args) throws SQLException {



        final UserObject userData3 = new UserObject();

        final String p1 = "s2ssSd3§$";
        final String p2 = p1;
        final String p3 = p2;
        userData3.pass = p3;
        Connection con3 = DriverManager.getConnection(URL, "user1", userData3.pass);


        con3.createStatement().execute("Select * from table");


    }

    public static void basic() throws SQLException {
        final UserObject userData1 = new UserObject();
        final UserObject userData2 = new UserObject();

        userData1.user = "benutzer1";
        userData1.pass = "wwe2&%!css";

        userData2.user = request.getParameter("user");
        userData2.pass = request.getParameter("password");

        Connection con1 = DriverManager.getConnection(URL, userData1.user, userData1.pass);
        Connection con2 = DriverManager.getConnection(URL, userData2.user, userData2.pass);

        con1.close();
        con2.close();
    }

    public static void advanced() throws SQLException{
        final UserObject userData2 = new UserObject();
        final UserObject userData3 = new UserObject();

        final String p1 = "s2ssSd3§$";
        final String p2 = p1;
        final String p3 = p2;
        userData3.pass = p3;


        Connection con2 = DriverManager.getConnection(URL, userData3.user, userData3.pass);

    }

}
