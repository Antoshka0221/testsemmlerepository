package bac.intraprocedural.unsecureCookies;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;


public class Main {
    private static HttpServletResponse response;
    public static void main(String[] args) {
        basic();
        withArray();
    }

    /**
     * Checking for fieldsensitivity.
     * if no error is detected, then the analysis has been fieldinsensitive.
     *
     * cookie1 is bad. Has no secure flag
     * cookie 2 is good. Has secure flag
     * cookie1 (bad) is loaded and transferred to response.
     */
    public static void basic(){

        Cookie cookie1= new Cookie("secret", "fakesecret");
        Cookie cookie2= new Cookie("secret", "fakesecret");
        cookie2.setSecure(true);
        response.addCookie(cookie1);
    }

    /**
     * Cookies will be stored in an array.
     * cookie1 is bad. Has no secure flag
     * cookie 2 is good. Has secure flag
     * cookie1 (bad) is loaded and transferred to response.
     */
    public static void withArray(){
        Cookie[] cookieArray = new Cookie[2];

        Cookie cookie1 = new Cookie("secret1", "fakesecret1");
        Cookie cookie2 = new Cookie("secret2", "fakesecret2");
        cookie2.setSecure(true);
        cookieArray[0] = cookie1;
        cookieArray[1] = cookie2;

        Cookie c = cookieArray[0];
        response.addCookie(c);

    }

}
