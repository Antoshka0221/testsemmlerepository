package bac.static_dynamic.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DynamicBindingSQL {

    public static void main(String[] args) throws SQLException {
Z a = new A();
Z b = new B();
Z c = new C();
Z d = new D();

        Connection con = DriverManager.getConnection("URL", "user", "pass");
        Statement st = con.createStatement();
st.execute("SELECT * FROM USERDATA WHERE userID = " + a.getId()); //bad
st.execute("SELECT * FROM USERDATA WHERE userID = " + b.getId()); //good
st.execute("SELECT * FROM USERDATA WHERE userID = " + c.getId()); //bad
st.execute("SELECT * FROM USERDATA WHERE userID = " + d.getId()); //bad


        E e = new E(d);
st.execute("SELECT * FROM USERDATA WHERE userID = " + e.getZ().getId()); //bad





    }

}
