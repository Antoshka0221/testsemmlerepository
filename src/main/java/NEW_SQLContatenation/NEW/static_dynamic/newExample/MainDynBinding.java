package NEW_SQLContatenation.NEW.static_dynamic.newExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MainDynBinding {
    public static void main(String[] args) throws SQLException {
        A a = new A();
        A b = new B();

        String s1 = a.f().getID(); //good
        String s2 = b.f().getID(); //bad

        Connection con = DriverManager.getConnection("URL", "user", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + s1);
        con.createStatement().execute("SELECT * FROM table WHERE id=" + s2);

    }
}
