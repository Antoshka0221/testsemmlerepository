package NEW_SQLContatenation.NEW.static_dynamic.hardcodedPassword;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class DynamicBindingSQL {

    public static void main(String[] args) throws SQLException {
        Z a = new A();
        Z b = new B();
        Z c = new C();
        Z d = new D();
        E e = new E(c);

        Connection con = DriverManager.getConnection("URL", "user", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + a.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + b.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + c.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + d.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + e.getID());


    }

    public static void test() throws SQLException {
        A a = new A();
        B b = new B();
        A c = new C();
        A d = new D();
        B e = new E(c);
        Connection con = DriverManager.getConnection("URL", "user", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + a.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + b.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + c.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + d.getID());
        con.createStatement().execute("SELECT * FROM table WHERE id=" + e.getID());
        con.close();
    }

}
