package NEW_SQLContatenation.NEW.inter.inter_function;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {


    static String URL = "localhost:8080/dbms";
    static HttpServletRequest request;
    public static void main(String[] args) throws SQLException {


        String username = "user";
establishConnection1(getPassword1());
establishConnection2(getPassword2());
establishConnection3(getPassword3());
establishConnection4(getPassword5());
establishConnection5(getPasswordLevel1());
establishConnectionToLevel_3(getPassword1());



    }

    public static void establishConnectionToLevel_3(String id) throws SQLException {
        establishConnectionLevel_2(id);
    }

    public static void establishConnectionLevel_2(String id) throws SQLException {
        establishConnectionLevel1(id);
    }

    public static void establishConnection1(String id) throws SQLException {
        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pass");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + id);
    }

    public static void establishConnection2(String id) throws SQLException {
        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pass");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + id);
    }

    public static void establishConnection3(String id) throws SQLException {
        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pass");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + id);
    }

    public static void establishConnection4(String id) throws SQLException {
        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pass");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + id);
    }

    public static void establishConnection5(String id) throws SQLException {
        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pass");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + id);
    }

    public static void establishConnectionLevel1(String id) throws SQLException {
        Connection con = DriverManager.getConnection("127.0.0.1", "username", "pass");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + id);
    }

    public static String getPassword1(){
        return request.getParameter("id");
    }

    //BAD: Password is hardcoded and returned by method
    public static String getPassword2(){
        return request.getParameter("id").toString();
    }

    //BAD: Password is hardcoded and returned by method using StringBuilder
    public static String getPassword3(){

        StringBuilder sb = new StringBuilder();
        sb.append(request.getParameter("id1"));
        sb.append(request.getParameter("id2"));
        sb.append(request.getParameter("id3"));
        return sb.toString();
    }

    public static String getPasswordLevel3(){
        return getPassword1();
    }
    public static String getPasswordLevel2(){
        return getPasswordLevel3();
    }
    public static String getPasswordLevel1(){
        return getPasswordLevel2();
    }


    /**
     * Kombination mit der feldsensitiven Analyse
     * @return
     */
    public static String getPassword5(){
        return UserIDFromUserInput.notiz;
    }

}
