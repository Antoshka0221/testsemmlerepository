package NEW_SQLContatenation.NEW.inter.context;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ContxtSens {
    static HttpServletRequest request;
    public static void main(String[] args) throws SQLException {
        easy();
        medium();
        mediumMultipleLevels();
    }

    /**
     * Soll entdeckt werden
     * Passwort wird direkt wieder zurückgegeben
     * @throws SQLException
     */
    static void easy() throws SQLException {
String s = request.getParameter("pswd");
String s2 = "2132$5%&%";
String x = getPasswordEasy(s);
String x2 = getPasswordEasy(s2);

Connection con =
DriverManager.getConnection("", "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + x);
        con.createStatement().execute("SELECT * FROM table WHERE id=" + x2);
    }


    /**
     * Soll entdeckt werden
     * Password wird über ein if/else zurückgegeben
     * @throws SQLException
     */
    static void medium() throws SQLException {
        String s = request.getParameter("pswd");
        String s2 = "2132$5%&%";
        String x = getPasswordMedium(s, true); //returns req.getParameter - bad
        String x2 = getPasswordMedium(s2, false); //returns value - good
        Connection con =
        DriverManager.getConnection("", "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + x);
        con.createStatement().execute("SELECT * FROM table WHERE id=" + x2);
    }

    /**
     * Soll entdeckt werden
     * Password wird über mehrere Methoden geleitet und anschließend wieder zurückgegeben.
     * @throws SQLException
     */
    static void mediumMultipleLevels() throws SQLException {
        String s = request.getParameter("pswd");
        String s2 = "2132$5%&%";
        String x = getPasswordLevel1(s); //bad
        String x2 = getPasswordLevel1(s2); //good
        Connection con =
                DriverManager.getConnection("", "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + x);
        con.createStatement().execute("SELECT * FROM table WHERE id=" + x2);
    }

    private static String getPasswordEasy(String s) {
        return s;
    }

    private static String getPasswordMedium(String s, boolean b) {
        if(b){
            return request.getParameter("pswd");
        }
        else{
            return s;
        }
    }

    private static String getPasswordLevel1(String s) {
        return getPasswordLevel2(s);
    }

    private static String getPasswordLevel2(String s) {
        return getPasswordLevel3(s);
    }

    private static String getPasswordLevel3(String s) {
        return s;
    }


}
