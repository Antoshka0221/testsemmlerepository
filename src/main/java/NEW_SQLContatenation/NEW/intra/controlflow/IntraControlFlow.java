package NEW_SQLContatenation.NEW.intra.controlflow;



import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class IntraControlFlow {

    static HttpServletRequest request;
    static String URL = "localhost:8080/dbms";
    public static void main(String[] args) throws SQLException {

        controlFlowTrivial();
        controlFlowSimple();
        controlFlowMedium();
        controlFlowHard(Boolean.parseBoolean(request.getParameter("ctf")));
        controlFlowAdvanced();
    }

    /**
     * Soll entdeckt werden, trivialer Fall
     * @throws SQLException
     */
    static void controlFlowTrivial() throws SQLException {
        String s = "2321"; //good
         s = request.getParameter("id"); //bad
Connection con = DriverManager.getConnection(URL, "", "");
con.createStatement().execute("SELECT * FROM table WHERE id=" + s);
    }
    /**
     * Soll nicht gefunden werden, da passwort aus parameter stammt
     * Ursprüngliches Passwort als hartcoded String wird überschrieben
     * Wenn gefunden: Kontrollflussinsensitiv
     */
    static void controlFlowSimple() throws SQLException {
        String s = request.getParameter("id"); //bad
        s = "2321"; //good
        Connection con = DriverManager.getConnection(URL, "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + s);
    }

    /**
     * Soll nicht gefunden werden, da passwort aus parameter stammt
     * Ursprüngliches Passwort als hartcoded String wird überschrieben
     * Mehrfaches hin- und her
     * Wenn gefunden: Kontrollflussinsensitiv
     */
    static void controlFlowMedium() throws SQLException {
String s = request.getParameter("id1");
doSomething();
        s = request.getParameter("id2");
        s = "345";
        s = request.getParameter("id3");
        s = "123";
        Connection con = DriverManager.getConnection(URL, "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + s);
        con.close();
    }

    /**
     * Kann gefunden werden
     * Im if Block, welcher immer Zutrifft, wird PW aus parameter geladen
     * Wenn gefunden: Werte werden komuliert werden, was bei if/else normal ist.
     */
    static void controlFlowHard(boolean b) throws SQLException {
        String s;
        doSomething();
        if(b){
            s = "qo2(%92929l";
        }else {
            s = request.getParameter("id");
        }
        Connection con = DriverManager.getConnection(URL, "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + s);
        con.close();
    }

    /**
     * Soll nicht gefunden werden
     * Password wird hartkodiert und über mehrere Variablen rübergereicht.
     * Wenn nicht gefunden, kontrollflusssensitivität nicht korrekt.
     */


    public static void controlFlowAdvanced() throws SQLException{


        final String p1 = request.getParameter("id");
        final String p2 = p1;
        final String p3 = p2;
        String s = p3;
        Connection con = DriverManager.getConnection(URL, "", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + s);
        con.close();
    }




    private static void doSomething() {
    }
}
