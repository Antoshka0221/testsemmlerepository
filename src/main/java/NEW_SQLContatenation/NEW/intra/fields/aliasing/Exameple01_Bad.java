package NEW_SQLContatenation.NEW.intra.fields.aliasing;


import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple01_Bad {

    static HttpServletRequest request = null;

    public static void main(String[] args) throws SQLException {
        String idBad = request.getParameter("id");
        String idGood = "42";

        Obj o1 = new Obj();
        Obj o2 = new Obj();
        o1.id = idGood; //good
        o2.id = idBad; //bad

        Connection con = DriverManager.getConnection("127.0.0.1", "username", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + o2.id);

        //Bad, because passowrd is from Obj z.

    }
}
