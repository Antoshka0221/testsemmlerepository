package NEW_SQLContatenation.NEW.intra.fields.aliasing;


import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple04_Bad {

    static HttpServletRequest request = null;
    public static void main(String[] args) throws SQLException {
        String pwHardCoded = request.getParameter("id");
        String pwUserInput = "42";


        Obj x = new Obj();
        Obj z = new Obj();
        x.id = pwUserInput; //good
        z.id = pwHardCoded; //bad

        Obj y = x;
        y.f = z;
        Obj v = y.f;

        Connection con = DriverManager.getConnection("127.0.0.1", "username", "");
        con.createStatement().execute("SELECT * FROM table WHERE id=" + v.id);


    }
}
