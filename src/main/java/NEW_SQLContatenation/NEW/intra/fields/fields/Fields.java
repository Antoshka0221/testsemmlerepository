package NEW_SQLContatenation.NEW.intra.fields.fields;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class Fields {
    static String URL = "localhost:8080/dbms";
    static HttpServletRequest request;


    public static void main(String[] args) throws SQLException {
        simple();
        basic();
        medium();
        hard();
    }

    /**
     * @ShouldFind
     *
     *
     * Basic case. Prüft ob die Feldsensitivität überhaupt vorhanden ist.
     *
     * @throws SQLException
     */
    public static void simple() throws SQLException {
        final UserObject userData1 = new UserObject();
        userData1.id = request.getParameter("id");
        Connection con1 = DriverManager.getConnection(URL, "", "");
        con1.createStatement().execute("SELECT * FROM table WHERE id=" + userData1.id);
        con1.close();
    }

    /**
     *
     * user1 soll unterstrichen werden, user 2 soll NICHT unterstrihcen werden.
     * Wenn auch user2 unterstrichen wird, dann ist die Analyse feldinsensitiv
     *
     * @throws SQLException
     */
    public static void basic() throws SQLException {
final UserObject userData1 = new UserObject();
final UserObject userData2 = new UserObject();

        userData1.id = request.getParameter("id");

        userData2.id = "42";

Connection con1 = DriverManager.getConnection(URL, "", "");
Connection con2 = DriverManager.getConnection(URL, "", "");
        con1.createStatement().execute("SELECT * FROM table WHERE id=" + userData1.id); //bad
        con1.createStatement().execute("SELECT * FROM table WHERE id=" + userData2.id); //good
        con1.close();
        con2.close();
    }

    public static void medium() throws SQLException {
final UserObjectA userObjectA1 = new UserObjectA();
userObjectA1.userObject.id = request.getParameter("id");
Connection con1 =
DriverManager.getConnection(URL, "", "");
        con1.createStatement().execute("SELECT * FROM table WHERE id=" + userObjectA1.userObject.id); //bad
        con1.close();
    }

    public static void hard() throws SQLException {
final UserObjectA userObjectA1 = new UserObjectA();
final UserObjectA userObjectA2 = new UserObjectA();

userObjectA1.userObject.id = request.getParameter("pswd");
userObjectA2.userObject.id = "as2$223";
userObjectA1.userObject.id = userObjectA2.userObject.id;

Connection con1 =
DriverManager.getConnection(URL, "", "");
        con1.createStatement().execute("SELECT * FROM table WHERE id=" + userObjectA1.userObject.id); //bad
        con1.close();
    }
}
