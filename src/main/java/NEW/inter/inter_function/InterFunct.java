package NEW.inter.inter_function;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class InterFunct {


    static String URL = "localhost:8080/dbms";

    public static void main(String[] args) throws SQLException {


        String username = "user";
establishConnection(URL, "", getPassword1());
establishConnection(URL, "", getPassword2());
establishConnection(URL, "", getPassword3());
establishConnection(URL, "", getPassword5());
establishConnection(URL, "", getPasswordLevel1());
establishConnectionToLevel_3(URL, "", getPassword1());



    }

    public static void establishConnectionToLevel_3(String url, String u, String p) throws SQLException {
        establishConnectionLevel_2(url, u, p);
    }

    public static void establishConnectionLevel_2(String url, String u, String p) throws SQLException {
        establishConnection(url, u, p);
    }

    public static void establishConnection(String url, String u, String p) throws SQLException {
        Connection c = DriverManager.getConnection(url, u, p);
        c.prepareStatement("SELECT * FROM TABLE");
        c.close();
    }

    public static String getPassword1(){
        return "passwort";
    }

    //BAD: Password is hardcoded and returned by method
    public static String getPassword2(){
        return "passwort".toString();
    }

    //BAD: Password is hardcoded and returned by method using StringBuilder
    public static String getPassword3(){

        StringBuilder sb = new StringBuilder();
        sb.append("Ts");
        sb.append("Tss");
        sb.append("Tssd");
        return sb.toString();
    }

    public static String getPasswordLevel3(){
        return getPassword1();
    }
    public static String getPasswordLevel2(){
        return getPasswordLevel3();
    }
    public static String getPasswordLevel1(){
        return getPasswordLevel2();
    }


    /**
     * Kombination mit der feldsensitiven Analyse
     * @return
     */
    public static String getPassword5(){
        return HardCodedPassword2.notiz;
    }

}
