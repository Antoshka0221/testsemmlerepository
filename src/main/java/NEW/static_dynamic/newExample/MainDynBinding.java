package NEW.static_dynamic.newExample;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MainDynBinding {
    public static void main(String[] args) throws SQLException {
A a = new A();
A b = new B();

String s1 = a.f().getPswd(); //good
String s2 = b.f().getPswd(); //bad

DriverManager.getConnection("", "", s1);
DriverManager.getConnection("", "", s2);


    }
}
