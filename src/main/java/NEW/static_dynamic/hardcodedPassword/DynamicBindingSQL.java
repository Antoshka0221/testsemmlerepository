package NEW.static_dynamic.hardcodedPassword;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class DynamicBindingSQL {

    public static void main(String[] args) throws SQLException {
        Z a = new A();
        Z b = new B();
        Z c = new C();
        Z d = new D();
        E e = new E(c);
        System.out.println(d.getPswd());
DriverManager.getConnection("URL", "user", a.getPswd());
DriverManager.getConnection("URL", "user", b.getPswd());
DriverManager.getConnection("URL", "user", c.getPswd());
DriverManager.getConnection("URL", "user", d.getPswd());
DriverManager.getConnection("URL", "user", e.getPswd());






    }

    public static void test() throws SQLException {
A a = new A();
B b = new B();
A c = new C();
A d = new D();
B e = new E(c);
        System.out.println(d.getPswd());
        Connection conA = DriverManager.getConnection("URL", "user", a.getPswd());
        Connection conB = DriverManager.getConnection("URL", "user", b.getPswd());
        Connection conC = DriverManager.getConnection("URL", "user", c.getPswd());
        Connection conD = DriverManager.getConnection("URL", "user", d.getPswd());
        Connection conE = DriverManager.getConnection("URL", "user", e.getPswd());

        conA.close();
        conB.close();
        conC.close();
        conD.close();
        conE.close();



    }

}
