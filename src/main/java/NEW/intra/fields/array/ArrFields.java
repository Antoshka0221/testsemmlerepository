package NEW.intra.fields.array;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class ArrFields {
    static String URL = "localhost:8080/dbms";
    static HttpServletRequest request;


    public static void main(String[] args) throws SQLException {
        basic();
        medium(Boolean.parseBoolean(request.getParameter("bln")));

    }

    public static void easy() throws SQLException {
final UserObject userData1 = new UserObject();
UserObject[] userObjects = new UserObject[1];

userData1.user = "benutzer1";
userData1.pass = "wwe2&%!css";

userObjects[0] = userData1;
UserObject userObjectArray1 = userObjects[0];

Connection con1 =
DriverManager.getConnection(URL, userObjectArray1.user, userObjectArray1.pass);
        con1.close();
    }

    /**
     * Hier wurden die zwei UserObjects, wie bei der Feldsensitivität erstellt.
     * Nur wurden nun die zwei UserObjects in einem Array gespeichert und anschließdn von dort aus geladen.
     *
     * @throws SQLException
     */
    public static void basic() throws SQLException {
final UserObject userData1 = new UserObject();
final UserObject userData2 = new UserObject();

UserObject[] userObjects = new UserObject[2];

userData1.user = "benutzer1";
userData1.pass = "wwe2&%!css";

userData2.user = request.getParameter("user");
userData2.pass = request.getParameter("password");

userObjects[0] = userData1;
userObjects[1] = userData2;


UserObject userObjectArray1 = userObjects[0];
UserObject userObjectArray2 = userObjects[1];

Connection con1 = DriverManager.
getConnection(URL, userObjectArray1.user, userObjectArray1.pass);
Connection con2 = DriverManager.
getConnection(URL, userObjectArray2.user, userObjectArray2.pass);

        con1.close();
        con2.close();
    }

    /**
     * Hier wurde noch die Kontrollflusssensitivität hinzugefügt.
     * Die Erstellung der Connection hängt von einer If-Bedingung ab.
     * Wenn true (immer), dann wird die "gefährliche" Verbindung erstellt
     * Sonst (nie) die ungefährliche
     * @throws SQLException
     */
    public static void medium(boolean b) throws SQLException {
        final UserObject userData1 = new UserObject();
        final UserObject userData2 = new UserObject();

        UserObject[] userObjects = new UserObject[2];

        userData1.user = "benutzer1";
        userData1.pass = "wwe2&%!css";

        userData2.user = request.getParameter("user");
        userData2.pass = request.getParameter("password");

        userObjects[0] = userData1;
        userObjects[1] = userData2;


        if(b){
            UserObject userObjectArray1 = userObjects[0];
            Connection con1 = DriverManager.getConnection(URL, userObjectArray1.user, userObjectArray1.pass);
            con1.close();
        }else {
            UserObject userObjectArray2 = userObjects[1];
            Connection con2 = DriverManager.getConnection(URL, userObjectArray2.user, userObjectArray2.pass);
            con2.close();
        }


    }

    /**
     * Muss gefunden werden
     * Schwierigkeit: Hier wurde der "schlechte" wert in ein Array gespeichert.
     * Dieses Array wurde in ein 2D Array gepackt
     * Der wert davon wird anschließend geladen und an den Parameter übergeben.
     * @throws SQLException
     */
    public static void hard() throws SQLException {
final UserObject userData1 = new UserObject();

UserObject[] userObjects = new UserObject[2];
UserObject[][] userObjects2 = {userObjects};

userData1.user = "benutzer1";
userData1.pass = "wwe2&%!css";

userObjects[0] = userData1;


UserObject userObjectArray1 = userObjects2[0][0];
Connection con1 = DriverManager.
getConnection(URL, userObjectArray1.user, userObjectArray1.pass);
        con1.close();


    }


}
