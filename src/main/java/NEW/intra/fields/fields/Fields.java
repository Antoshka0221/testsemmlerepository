package NEW.intra.fields.fields;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class Fields {
    static String URL = "localhost:8080/dbms";
    static HttpServletRequest request;


    public static void main(String[] args) throws SQLException {
        simple();
        basic();
        medium();
        hard();
    }

    /**
     * @ShouldFind
     *
     *
     * Basic case. Prüft ob die Feldsensitivität überhaupt vorhanden ist.
     *
     * @throws SQLException
     */
    public static void simple() throws SQLException {
        final UserObject userData1 = new UserObject();
        userData1.user = "benutzer1";
        userData1.pass = "wwe2&%!css";
        Connection con1 = DriverManager.getConnection(URL, userData1.user, userData1.pass);
        con1.close();
    }

    /**
     *
     * user1 soll unterstrichen werden, user 2 soll NICHT unterstrihcen werden.
     * Wenn auch user2 unterstrichen wird, dann ist die Analyse feldinsensitiv
     *
     * @throws SQLException
     */
    public static void basic() throws SQLException {
final UserObject userData1 = new UserObject();
final UserObject userData2 = new UserObject();

userData1.user = "benutzer1";
userData1.pass = "wwe2&%!css";

userData2.user = request.getParameter("user");
userData2.pass = request.getParameter("password");

Connection con1 = DriverManager.getConnection(URL, userData1.user, userData1.pass);
Connection con2 = DriverManager.getConnection(URL, userData2.user, userData2.pass);

        con1.close();
        con2.close();
    }

    public static void medium() throws SQLException {
final UserObjectA userObjectA1 = new UserObjectA();
userObjectA1.userObject.pass = "as2$223";
Connection con1 =
DriverManager.getConnection(URL, "", userObjectA1.userObject.pass);

        con1.close();
    }

    public static void hard() throws SQLException {
final UserObjectA userObjectA1 = new UserObjectA();
final UserObjectA userObjectA2 = new UserObjectA();

userObjectA1.userObject.pass = "as2$223";
userObjectA2.userObject.pass = request.getParameter("pswd");
userObjectA1.userObject.pass = userObjectA2.userObject.pass;

Connection con1 =
DriverManager.getConnection(URL, "", userObjectA1.userObject.pass);

        con1.close();
    }
}
