package NEW.intra.fields.aliasing;


import javax.servlet.http.HttpServletRequest;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Exameple02_Good {

    static HttpServletRequest request = null;

    public static void main(String[] args) throws SQLException {
        String pwHardCoded = "unsicheresPasswort123";
        String pwUserInput = request.getParameter("pass");

        Obj o1 = new Obj();
        Obj o2 = new Obj();
        o1.password = pwUserInput; //good
        o2.password = pwHardCoded; //bad

        DriverManager.getConnection("127.0.0.1", "username", o1.password);

        //Bad, because passowrd is from Obj z.

    }
}
