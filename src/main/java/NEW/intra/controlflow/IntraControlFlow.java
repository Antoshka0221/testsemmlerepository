package NEW.intra.controlflow;



import javax.servlet.http.HttpServletRequest;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class IntraControlFlow {

    static HttpServletRequest request;
    static String URL = "localhost:8080/dbms";
    public static void main(String[] args) throws SQLException {

        controlFlowTrivial();
        controlFlowSimple();
        controlFlowMedium();
        controlFlowHard(Boolean.parseBoolean(request.getParameter("ctf")));
        controlFlowAdvanced();
    }

    /**
     * Soll entdeckt werden, trivialer Fall
     * @throws SQLException
     */
    static void controlFlowTrivial() throws SQLException {
String s = request.getParameter("pw");
s = "2321";
DriverManager.getConnection(URL, "", s);
    }
    /**
     * Soll nicht gefunden werden, da passwort aus parameter stammt
     * Ursprüngliches Passwort als hartcoded String wird überschrieben
     * Wenn gefunden: Kontrollflussinsensitiv
     */
    static void controlFlowSimple() throws SQLException {
String s = "2321";
s = request.getParameter("pw");
DriverManager.getConnection(URL, "", s);
    }

    /**
     * Soll nicht gefunden werden, da passwort aus parameter stammt
     * Ursprüngliches Passwort als hartcoded String wird überschrieben
     * Mehrfaches hin- und her
     * Wenn gefunden: Kontrollflussinsensitiv
     */
    static void controlFlowMedium() throws SQLException {
String s = "2321";
doSomething();
s = "soq1(!§%%1!%;";
s = request.getParameter("pw");
s = "qo2(%92929l";
s = request.getParameter("pw");
Connection c = DriverManager.getConnection(URL, "", s);

        c.close();
    }

    /**
     * Kann gefunden werden (sollte besser nicht)
     * Im if Block, welcher immer Zutrifft, wird PW aus parameter geladen
     * Wenn gefunden: Werte werden komuliert werden, was bei if/else normal ist.
     */
    static void controlFlowHard(boolean b) throws SQLException {
String s;
doSomething();
if(b){
    s = request.getParameter("pw");
}else {
    s = "qo2(%92929l";
}
Connection c = DriverManager.getConnection(URL, "", s);
        c.close();
    }

    /**
     * Soll gefunden werden
     * Password wird hartkodiert und über mehrere Variablen rübergereicht.
     * Wenn nicht gefunden, kontrollflusssensitivität nicht korrekt.
     */
    public static void controlFlowAdvanced() throws SQLException{


final String p1 = "s2ssSd3§$";
final String p2 = p1;
final String p3 = p2;
String s = p3;
Connection c = DriverManager.getConnection(URL, "", s);
        c.close();
    }




    private static void doSomething() {
    }
}
